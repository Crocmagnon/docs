# Déframasoftiser Framemo

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos tableaux vous devez&nbsp;:

  * cliquer sur l'icône <i class="fa fa-exchange" aria-hidden="true"></i>
  * cliquer sur **Format JSON (pour import dans Framemo)**
  * enregistrer le fichier sur votre ordinateur

## Importer

Pour importer vos tableaux vous devez utiliser le fichier précédemment exporter (voir ci-dessus). Puis, vous devez&nbsp;:

  * cliquer sur l'icône <i class="fa fa-exchange" aria-hidden="true"></i>
  * cliquer sur **Parcourir…** dans la section **Importer un tableau**
  * récupérer le fichier `le-nom-de-votre-tableau.json`
  * cliquer sur le bouton **Importer**
  * cliquer sur l'onglet **Tableau** pour voir votre import

## Supprimer un tableau

<div class="alert-info alert">Il n’y a pas de suppression à proprement dite : il suffit de supprimer les notes (et les révisions le cas échéant). Si quelqu’un réutilise la même adresse que votre mémo, il tombera sur un tableau vide.</div>

### Supprimer une note

Pour supprimer une note, vous devez&nbsp;:

  * passer la souris sur la note
  * cliquer sur l'icône <i class="fa fa-times" aria-hidden="true"></i> qui apparaît

### Supprimer un tableau

Pour supprimer un tableau, vous devez&nbsp;:

  * cliquer sur l'icône <i class="fa fa-history" aria-hidden="true"></i>
  * cliquer sur l'icône <i class="fa fa-ban" aria-hidden="true"></i> devant chaque révision
