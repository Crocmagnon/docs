# Inviter de nouveaux membres

![image invit](images/loomio-invit.png)


## Invitation individuelle

Selon les [paramètres de visibilité de votre groupe](group_settings.html), les nouveaux membres peuvent être invités soit par chaque membre du groupe, ou seulement par les coordinateurs. Vous trouverez l‎‎’option **Inviter des personnes** au niveau de la boîte **Membres** sur la page de votre groupe, dans l'onglet **Membres** et sur la page des discussions.

## Lien d'invitation

Pour partager le lien d'invitation vous devez cliquer sur le bouton **Partager**. Les gens peuvent demander à s’y joindre et vous pouvez approuver ou ignorer les demandes.

Vous avez le choix entre&nbsp;:

  * Lien du groupe : vous pouvez partager ce lien publiquement. Les gens peuvent demander à s’y joindre et vous pouvez approuver ou ignorer les demandes
  * Lien d’invitation réutilisable : ne partagez pas ce lien publiquement. Toute personne en possession de ce lien peut rejoindre votre groupe sans votre approbation. Ne le partagez que dans le cadre de communications privées pour permettre aux membres de votre organisation de vous rejoindre

## Invitations en attente

Si, exceptionnellement, une invitation n‎‎’a pas été encore acceptée, elle sera mentionnée dans la section **Invitations** de l'onglet **Membres**. Vous pouvez alors renvoyer ou annuler l'invitation en cliquant sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> à côté de l'adresse mail invitée.
