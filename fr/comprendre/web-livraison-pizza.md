# Le Web, c’est comme une livraison de Pizza (ou presque)

Issu de https://blog.seboss666.info/2016/02/le-web-cest-comme-une-livraison-de-pizza/ ([Creative Commons BY-NC v3.0](https://creativecommons.org/licenses/by-nc/3.0/fr/))

## Internet

Dit le réseau des réseaux, pourquoi ? Prenons une Livebox (box pourrie mais la plus répandue de France). Tout ce qui se trouve relié, en Wifi ou en Ethernet, à cette boite constitue un réseau, dit local. Ce réseau est ensuite relié à celui d’Orange. Et c’est pareil chez tout le monde, et chez toutes les sociétés, les centres de données, et j’en passe. Internet, c’est la somme de tous ces réseaux. N’importe quel ordinateur sur tous ces réseaux peut jouer le même rôle, ce qu’on appelle un serveur n’est rien de plus qu’un ordinateur, même si la plupart du temps il n’y a pas d’affichage. Un ordinateur est identifié par une adresse IP, et pour aujourd’hui on va dire qu’elle est unique. Et les ordinateurs ne communiquent entre eux qu’avec cette adresse IP.
Le modèle client/serveur

C’est rapide à expliquer, c’est exactement comme quand vous allez au restaurant, vous êtes le client, vous demandez une Pizza, le serveur vous la sert. Et ça tombe bien, puisque mon analogie d’aujourd’hui tourne autour de la pizza, mais le serveur vous la livre à domicile. Notamment si comme moi vous avez une forte tendance à la feignantise 😀

C’est parti pour les choses sérieuses, on sort les carapaces !

## La version « bouffe »

Que se passe-t-il quand vous avez faim ? Vous voulez commander chez la seule pizzeria de la région qui livre chez vous. Seulement vous ne connaissez pas le numéro de téléphone, et par malheur vous n’avez plus le dépliant avec la liste des pizzas. Vous appelez donc les Pages Jaunes, service bien connu qui vous transmet le numéro. Vous appelez, confirmez que c’est bien la pizzeria, vous demandez à vous faire livrer une Calzone. Le pizzaïolo la prépare, la fait cuire, et la met en boite pour que le livreur vous l’apporte. Parfait, vous pouvez passer à table.

## La version informatique

Pareil, on va prendre un exemple trop courant à mon goût en France. Vous demandez à afficher la page d’accueil (la pizza) de FaceBook (la pizzeria). Le navigateur/application (Firefox, Safari, Internet Explorer) demande au système d’exploitation (Windows, iOS, Android, Linux!) de se renseigner sur l’adresse IP qu’il y a derrière le nom (le couple navigateur/système d’exploitation, c’est vous). Il fait appel pour ça à un serveur dit DNS (système de nom de domaine), qui peut être soit celui de votre opérateur, celui de votre box (qui se repose souvent sur celui de votre opérateur) ou celui de votre choix. Le DNS sert principalement à dire quelle adresse IP contacter pour le nom de domaine demandé. Les DNS, ce sont les Pages Jaunes.

Une fois qu’il a l’adresse IP, il contacte l’ordinateur qui correspond avec un protocole, HTTP(S) (le téléphone), pour lui demander s’il prend en charge facebook.com. C’est bien le cas ? Cool, on peut demander ce qu’on veut.

Vous demandez donc la page d’accueil du site (une Calzone). Pour la préparer selon vôtre goût, le serveur cherche les composants de la page : les derniers messages de vos « amis », vos messages à vous, les notifications des jeux (les ingrédients de la pizza : pâte à pizza/sauce tomate/jambon/œuf), et j’en passe. Toutes ces informations sont stockées dans des bases de données (des recueils de recette). Une fois la page d’accueil prête, le serveur la délivre.

Le navigateur reçoit la page et vous l’affiche. Cette page n’est rien de plus que du texte, et c’est là qu’on touche aux limites de l’analogie. En effet, la page n’est en fait qu’une partie de la recette,  la pâte avec la liste des ingrédients mais avec les liens pour avoir les éléments manquants, comme les images (les olives sur la Pizza). Récupérer ces images dépend de leur lieu : soit sur le même domaine (la même pizzeria), auquel cas on a déjà l’adresse IP (le numéro de téléphone) pour les rappeler afin qu’ils les livrent également. Soit les images/olives se trouvent ailleurs, pourquoi pas chez votre voisin, et il faut alors faire une nouvelle requête DNS (trouver le numéro du voisin) pour lui demander les fameuses images (le pot qui se trouve dans son frigo quoi).

## Le web c’est pratique, et ça devient vital

Pratique, parce qu’on peut maintenant en faire de partout, c’est jusque dans notre poche. C’est pas parfait, les opérateurs font des choses pas propres, je vous passe les détails, mais ça marche quand même bien la plupart du temps.

Et il y a intérêt, avec nos mairies qui vont proposer de réaliser de plus en plus de démarches en ligne uniquement, à l’image de l’Assurance Maladie avec AMELI. Et il est vrai, qu’en prenant mon exemple du « parti à 8h, rentré à 20h », j’ai peu de chance de pouvoir me rendre à la mairie de ma ville, pouvoir faire ces démarches sur le Web est un gros plus. Et en bonus, c’est simple !

Sans parler du fait que grâce au Web, tout le monde a réellement le droit de s’exprimer, ce qui est une des libertés fondamentales évoquées par la déclaration des droits de l’Homme, et encore aujourd’hui, on doit se battre pour conserver un droit qui n’était que théorique avant l’apparition d’Internet.

## Héberger votre bout de Web ?

Pour les plus curieux, rappelez vous que j’ai dit au départ que n’importe quel ordinateur sur Internet peut jouer tous les rôles au dessus. on a évoqué le DNS, le serveur Web, la base de données, vous pouvez installer tout ça chez vous pour jouer avec, ou si vous avez une bonne connexion (c’est probablement pas encore le cas), vous pouvez carrément héberger votre site chez vous :)

Exemple, que j’ai d’ailleurs déjà présenté dans un autre billet, j’ai beaucoup de films en DVD et Bluray, et j’ai écrit un programme, sous forme de site/application Web, pour les enregistrer et vérifier, quand je suis au rayon DVD à Auchan, si j’ai déjà le film, ou suivre ceux que j’ai prêté. Je suis également un gros mangeur d’informations (par flux RSS), pour pouvoir suivre l’actualité à ma façon et quelque soit le lieu et l’appareil, je me suis tourné vers une application Web, pour le coup hébergée à côté du blog.

Certes c’est compliqué, mais des gens se bougent pour que justement, vous n’ayez pas à vous embêter avec la complexité tout en gardant le contrôle, ce que ne permettent pas la plupart des gros services actuels, tous hébergés à l’étranger et trop souvent hors de portée juridique. Comme Framasoft par exemple, association qui vous propose d’utiliser d’abord chez eux quantité de services, services que vous pouvez également installer vous-même plus tard si le cœur vous en dit. Framasoft qui devrait bientôt annoncer un collectif d’hébergeurs associatifs, les C.H.A.T.O.N.S. (acronyme véridique, après tout, c’est Internet) qui justement vous respecteront et s’occuperont des difficultés techniques pour vous.

Sinon, en attendant, quelqu’un est partant pour une Pizza ?
