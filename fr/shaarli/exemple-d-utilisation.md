# Exemple d’utilisation de MyFrama

<div class="alert-warning alert">
<b class="violet">My</b><b class="vert">Frama</b> fermera ses portes le <a href="https://framablog.org/2020/03/03/10-bonnes-raisons-de-fermer-certains-services-framasoft-la-5e-est-un-peu-bizarre/">mardi 12 janvier 2021</a>. Vous trouverez un service similaire <a href="https://alt.framasoft.org/fr/myframa">sur cette page</a>.<br>
La création de compte est dorénavant désactivée. Si vous aviez déjà un compte vous pouvez toujours <a href="deframasoftiser.html">récupérer vos données</a>.
</div>

## Concrètement, je fais comment pour utiliser MyFrama&nbsp;?

La première étape, c’est de se créer son compte
! Vous allez sur [my.framasoft.org](https://my.framasoft.org), et vous
cliquez sur «&nbsp;Créer un compte&nbsp;» pour entrer vos informations&nbsp;:

Voilà, votre compte est créé, il vous suffit de taper votre mot de passe une
seconde fois pour vous y connecter (par contre ne cochez la case «&nbsp;rester
connecté&nbsp;» **que** si vous êtes sur votre ordinateur perso). Notez que
votre nom d’utilisateur est passé en «&nbsp;tout en minuscules&nbsp;» (beaucoup plus
facile à retenir ^^)

![myframa-connection](images/myframa-connection.png)

Vous arrivez donc sur votre compte MyFrama, où nous vous avons
pré-rempli quelques filtres et liens pour l’exemple. Regardons cela
ensemble&nbsp;:

![myframa-complet](images/myframa-complet.png)


### 1) la barre d’outils

Elle vous permet de&nbsp;:

-   <i class="fa fa-search"></i> rechercher un lien parmi vos favoris&nbsp;;
-   <i class="fa fa-eye"></i> régler l’affichage des liens&nbsp;;
-   <i class="fa fa-rss"></i> obtenir le flux RSS de vos liens&nbsp;;
-   <i class="fa fa-cog"></i> gérer vos paramètres&nbsp;;
-   <i class="fa fa-power-off"></i> se déconnecter de MyFrama.

### 2) ajouter des liens

Il y a plusieurs moyens d’ajouter des adresses web dans votre MyFrama.
Le premier est de la copier puis la coller dans la grande barre en haut
de de l’accueil (cadre 2). Le deuxième est d’aller dans vos paramètres
(bouton <i class="fa fa-cog"></i>) pour ajouter un des boutons suivants à votre navigateur
préféré&nbsp;:
![Yapluka suivre ce qui est écrit \^\^!](images/myframa-boutons-partage.png)
Le
dernier c’est d’utiliser une application sur votre mobile. Pour Android,
vous avez l’application Shaarlier, qui est disponible sur [le magasin libre Fdroid](https://f-droid.org/repository/browse/?%20fdid=com.dimtion.shaarlier)
et sur le [Playstore de Google](https://play.google.com/store/apps/details?id=com.dimtion.shaarlier&hl=fr).

![shaarlier-sur-android](images/shaarlier-sur-android.png)

Pensez à préciser&nbsp;:

-   L’url de votre shaarli&nbsp;: `https://my.framasoft.org/u/votrepseudo`
-   Pseudo&nbsp;: en minuscule
-   Mot de passe (sans se tromper ^^)
-   Le nom du compte&nbsp;: répétez votre pseudo en minuscule&hellip;

Voici le résultat sous vos yeux ébaubis.  

### 3) 4) et 5) lorsque vous ajoutez un lien

Un lien est une adresse web (URL), et afin de la retrouver plus
facilement dans votre fourre-tout, vous pouvez en préciser&nbsp;:

-   Le titre (cadre 3)
-   La description (cadre 4)
-   Des étiquettes (les «&nbsp;tags&nbsp;», cadre 5)
-   Et si vous voulez qu’il soit privé ou public.

![myframa-ajout-lien](images/myframa-ajout-lien.png)

### 6) des filtres automatiques pour retrouver vos services Framasoft

On vient de partager un Framapad avec vous&nbsp;? Vous voulez mettre de côté
le Framadate de votre prochaine réunion d’équipe&nbsp;? Pas de soucis&nbsp;: allez
sur la page en question, et cliquez sur le bouton violet MyFrama dans la
Framanav (la barre tout en haut)
![Encore une fois&nbsp;: juste comme ça&nbsp;;)](images/myFrama-comme-ca.png)

Nous avons pré-réglé des
filtres automatiques pour que votre compte MyFrama reconnaisse
automatiquement les adresses `framapad.org` ou `framadate.org` etc. et
leur attribue une étiquette correspondante. Une fois dans votre compte,
il vous suffit de cliquer sur le bon tag (cadre 6) pour y retrouver vos
liens&nbsp;!

### 7) des filtres que vous pouvez modifier à loisir

Bien entendu, vous restez libre de gérer le tri automatique de vos
favoris&nbsp;! Pour cela&nbsp;:

-   rendez-vous dans les paramètres des tags (bouton en bas à droite)&nbsp;;
-   indiquez l’étiquette que vous voulez dans la colonne «&nbsp;Nom&nbsp;» (ici
    `korben`)&nbsp;;
-   et éventuellement le motif que MyFrama doit repérer pour trier
    automatiquement (ici `korben.info`)&nbsp;;
-   déterminez l’ordre avec les flèches de gauche&nbsp;;
-   cochez/décochez les options à droite (page d’accueil/lien privé).


![myframa-filtres](images/myframa-filtres.png)

Et voilà, vous n’avez plus qu’à vous créer votre petit fourre-tout du
web avec MyFrama&nbsp;!
