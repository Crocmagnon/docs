# Activités sur le projet

Une autre section du projet montre ce qui s'y passe. Allez dans la section « _Activity_ »

![](images/Activity-1.png)


Où l'on peut voir aisément les activités récentes concernant ce projet.


## Faisons le point

Maintenant vous avez appris les bases pour la gestion documentaire en collaboratif sous Framagit, et si vous désirez aller plus loin voir la documentation anglaise sur **[GitLab](https://framagit.org/help)**.

Vous pouvez aussi prolonger votre connaissance de Framagit en lisant ces deux articles très intéressants :

- [Framagit : une forge que j’ai découverte bien trop tard !](https://ordinosor.wordpress.com/2017/04/12/framagit-une-forge-que-jai-decouverte-bien-trop-tard/)
- [Notre GitLab évolue en Framagit. C’est très efficace !](https://framablog.org/2016/04/19/notre-gitlab-evolue-en-framagit-cest-tres-efficace/)
