# Astuces

## Je veux afficher des gifs directement dans Framateam

Pour cela, vous devez créer une commande slash.

Rendez vous dans le menu > **Intégrations** > **Commande slash** > **Ajouter une commande slash** :

  * **Titre** : Recherche et affiche un gif de Giphy
  * **Description** : Permet d'afficher un gif depuis le site giphy directement dans le salon
  * **Mot-clé de déclenchement** : gif
  * **URL de requête** : `https://giphy.framasoft.org`
  * **Méthode de requête** : POST
  * **Auto-complétion** cochée
  * **Indice de l’auto-complétion** : [texte de recherche]
  * **Description de l'auto-complétion** : Permet d'afficher un gif depuis le site giphy directement dans le salon

Ensuite, cliquez sur  **Enregistrer** puis **Effectué** puis **Retour sur Framateam**.

Désormais, vous pouvez taper la commande `/gif hello` puis `Entrée` pour faire apparaître un gif en rapport avec la demande `Hello` depuis le site giphy directement dans le salon. Celui-ci étant en anglais, les textes de recherches doivent se faire en anglais.

## Afficher les notifications Framaboard dans Framateam

<p class="alert-info alert">Vous devez être administrateur/administratrice de l'équipe Framateam et du projet Framaboard</p>

Vous avez la possibilité d'afficher les notifications de Framaboard dans un canal Framateam. Pour ce faire, vous devez&nbsp;:

**Dans Framateam**

  1. cliquer sur l'icône <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur **Intégrations**
  * cliquer sur **Webhooks entrants**
  * cliquer sur le bouton **Ajouter des Webhooks entrants**
  * spécifier un nom ("Notification board" par exemple)
  * spécifier une description
  * sélectionner un canal dans lequel seront envoyés les notifications
  * cliquer sur le bouton **Enregistrer**
  * cliquer sur <i class="fa fa-files-o" aria-hidden="true"></i> pour copier l'url du Webhooks (de type `https://framateam.org/hooks/CHIFFRES-LETTRES`)

**Dans Framaboard**

  1. cliquer sur <i class="fa fa-cog" aria-hidden="true"></i>
  * cliquer sur <i class="fa fa-cog" aria-hidden="true"></i> **Configurer ce projet**
  * cliquer sur **Intégrations** dans la colonne latérale gauche
  * coller l'url du Webhooks Framateam dans le champ **URL du webhook** dans la section **Mattermost**
  * cliquer sur le bouton **Enregistrer**
  * cliquer sur **Notifications** dans la colonne latérale gauche
  * cocher **Mattermost**
  * cliquer sur **Enregistrer**


## Afficher les notifications Framagit dans Framateam

<p class="alert-info alert">Vous devez être administrateur/administratrice de l'équipe Framateam et du projet Framagit</p>

Vous avez la possibilité d'afficher les notifications de Framagit dans un canal Framateam. Pour ce faire, vous devez&nbsp;:

**Dans Framateam**

  1. cliquer sur l'icône <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur **Intégrations**
  * cliquer sur **Webhooks entrants**
  * cliquer sur le bouton **Ajouter des Webhooks entrants**
  * spécifier un nom ("Notification board" par exemple)
  * spécifier une description
  * sélectionner un canal dans lequel seront envoyés les notifications
  * cliquer sur le bouton **Enregistrer**
  * cliquer sur <i class="fa fa-files-o" aria-hidden="true"></i> pour copier l'url du Webhooks (de type `https://framateam.org/hooks/CHIFFRES-LETTRES`)

**Dans Framagit**

  1. aller dans **Paramètres** > **Intégrations**
  * cliquer sur **Mattermost notifications** dans la section **Project services**
  * cocher la case **Active**
  * coller l'URL webhook précédemment générée dans le champ **Webhook**
  * cliquer sur le bouton **Test settings and save changes**

Vous recevrez alors une notification de test dans le canal choisi.
