# Modération sur Mastodon (Framapiaf)

## Vous protéger rapidement

Si quelqu’un vous insupporte ou si vous vous sentez inconfortable dans une situation, vous avez accès à plusieurs possibilités pour éviter d’y être confronté·e.

### Masquer la conversation

Si vous êtes cité·e dans une conversation et que vous ne voulez plus en recevoir les notifications, vous pouvez **Masquer la conversation** en accédant au menu sous chaque pouet, via l’icône <i class="fa fa-ellipsis-h" aria-hidden="true"></i>&nbsp;:

![Image montrant comment masquer une conversation](images/mastodon_moderation_masquer_conversation.png)

### Masquer ou bloquer une personne

Vous pouvez masquer le contenu d’une personne (notifications incluses ou non, à vous de choisir après avoir cliqué) ou bloquer cette personne (afin qu’elle ne puisse plus voir vos contenus) en accédant au menu sous chaque pouet, via l’icône <i class="fa fa-ellipsis-h" aria-hidden="true"></i>&nbsp;:

![image montrant comment masquer ou bloquer une personne](images/mastodon_moderation_masquer_personne.png)

Vous avez accès au même menu directement sur la page profil de cette personne, à droite de sa photo de profil, en cliquant sur l’icône <i class="fa fa-ellipsis-v" aria-hidden="true"></i>&nbsp;:

![image montrant comment masquer ou bloquer une personne](images/mastodon_moderation_masquer_personne2.png)

### Masquer ou bloquer une instance

Si vous le souhaitez, vous pouvez aussi bloquer une **instance entière**. Pour cela, rendez-vous sur le profil d’une personne appartement à cette instance, et dans le menu à droite de sa photo de profil en cliquant sur l'icône <i class="fa fa-ellipsis-v" aria-hidden="true"></i>, sélectionnez **Tout masquer venant de nomdelinstance.org**.

![image montrant comment masquer tout le contenu venant d'une instance](images/mastodon_moderation_tout_masquer_instance.png)

### Paramétrer vos notifications et l’accès aux messages directs

Dans les paramètres de votre compte Mastodon, section **Notifications**, vous pouvez choisir de&nbsp;:

  * Masquer les notifications des personnes qui ne vous suivent pas
  * Masquer les notifications des personnes que vous ne suivez pas
  * Bloquer les messages directs des personnes que vous ne suivez pas

![image montrant les paramètres de notifications](images/mastodon_moderation_notifs.png)


## Signaler à l’équipe de modération

Si vous rencontrez un contenu qui va à l’encontre de <a href="https://framasoft.org/fr/moderation/" title="lien vers la charte de modération">la charte de modération</a> et que celui-ci **nécessite une action de la modération** afin de **vous protéger, ou pour que d’autres utilisateur·ices n’y soient pas confronté·es**, vous pouvez sous chaque pouet, accéder au menu où se trouve l’option en cliquant sur l'icône <i class="fa fa-ellipsis-h" aria-hidden="true"></i> puis sur “Signaler @PseudoFramapiaf”&nbsp;:

![image montrant comment signaler un compte depuis un pouet](images/mastodon_moderation_signaler_user.png)

Ce menu se trouve aussi sur le profil des membres du *fédiverse*&nbsp; en cliquant sur l'icône <i class="fa fa-ellipsis-v" aria-hidden="true"></i>:

![image montrant comment signaler un compte depuis la page de profil](images/mastodon_moderation_signaler_user2.png)

Une fois cliqué sur “Signaler” vous aurez accès à une pop-up vous permettant de&nbsp;:

  * Nous écrire afin de nous préciser quels points de <a href="https://framasoft.org/fr/moderation/" title="lien vers la charte de modération">la charte</a> le contenu signalé ne respecte pas
  * Cocher un interrupteur pour envoyer ce rapport à l’administrateur de l’instance du compte signalé (si ce compte est sur une autre instance)
  * Cocher les pouets que vous souhaitez signaler, pour pouvoir nous indiquer en une fois quels contenus nécessitent une action de notre part.

![image montrant la fenêtre de signalement](images/mastodon_moderation_fenetre_signalement.png)

Votre signalement sera ensuite envoyé à l’équipe de modération bénévole.
