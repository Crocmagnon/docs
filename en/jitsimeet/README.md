# Framatalk

[Framatalk](https://framatalk.org/) is a free online service that allows you to create a videoconference room and invite your contacts.

Powered by the free software [Jitsi Meet](https://jitsi.org/Projects/JitsiMeet), Framatalk offers many options:

  * A chat to chat in text mode (you will need to enter a nickname);
  * A conversation invitation button (sharing the lounge's web address by email);
  * Buttons to enable/disable the microphone, camera, full screen mode;
  * Access to the parameters (modify your nickname, camera, microphone);
  * The possibility that Framatalk will retain your parameter profile (it will create a cookie);
  * Moderation rights of the lounge (for the first person to arrive);
  * The possibility to protect the lounge by password (for the moderator).


## See more:

  * [How to…?](how-to.html)
  * [Try Framatalk](https://framatalk.org/)
  * [Use Jitsi Meet instance](https://meet.jit.si/)
  * Part of [the De-google-ify Internet campaign](https://degooglisons-internet.org/en/)
